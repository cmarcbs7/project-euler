{- Project Euler Solutions in Haskell -}

import Data.Char
import Data.List

------------------------------------------------------------------------------
-- Interface
------------------------------------------------------------------------------

solve :: Integer -> Maybe Integer
solve problem_id =
    case problem_id of
        1   -> Just solve1
        2   -> Just solve2
        3   -> Just solve3
        4   -> Just solve4
        5   -> Just solve5
        6   -> Just solve6
        7   -> Just solve7
        8   -> Just solve8
        9   -> Just solve9
        20  -> Just solve20
        _   -> Nothing

------------------------------------------------------------------------------
-- Common Utilities
------------------------------------------------------------------------------

{- Convert a decimal character to the corresponding integer -}
digitToInteger :: Char -> Integer
digitToInteger c =
    toInteger (digitToInt c)

{- Calculate a number's digital sum -}
digitalSum :: Integer -> Integer
digitalSum n =
    sum (map digitToInteger (show n))

{- Calculate a number's digital root -}
digitalRoot :: Integer -> Integer
digitalRoot n =
    let
        digits = map digitToInteger (show n)
    in
        if length digits == 1 then
            head digits
        else
            digitalRoot (sum digits)

{- Calculate n factorial -}
fac :: Integer -> Integer
fac n =
    if n > 0 then
        (fac (n - 1)) * n
    else
        1

------------------------------------------------------------------------------
-- Problem 1
------------------------------------------------------------------------------

solve1 :: Integer
solve1 = (sum [0,3..999]) + (sum [0,5..999]) - (sum [0,15..999])

------------------------------------------------------------------------------
-- Problem 2
------------------------------------------------------------------------------

sumEvenFib :: Integer -> Integer -> Integer
sumEvenFib last current =
    if current > 4000000 then
        0
    else if (even current) then
        current + (sumEvenFib current (last + current))
    else
        sumEvenFib current (last + current)

solve2 :: Integer
solve2 = sumEvenFib 1 1

------------------------------------------------------------------------------
-- Problem 3
------------------------------------------------------------------------------

trialDiv :: Integer -> Integer -> Integer
trialDiv x divisor =
    if x == divisor then
        x
    else if x `mod` divisor == 0 then
        trialDiv (x `div` divisor) divisor
    else
        trialDiv x (divisor + 1)

solve3 :: Integer
solve3 =
    trialDiv 600851475143 1

------------------------------------------------------------------------------
-- Problem 4
------------------------------------------------------------------------------

palindromic :: Integer -> Bool
palindromic x =
    let
        s = show x
    in
        s == reverse s

biggestBoi :: [Integer] -> Maybe Integer
biggestBoi ls =
    case ls of
        [] ->
            Nothing
        head:[] ->
            Just head
        head:tail ->
            case biggestBoi tail of
                Nothing ->
                    Just head
                Just x ->
                    Just (max head x)

solve4 :: Integer
solve4 =
    let
        products = [x * y | x <- [100..999], y <- [100..999]]
    in
        case biggestBoi [x | x <- products, palindromic x] of
            Just answer ->
                answer
            _ ->
                0

------------------------------------------------------------------------------
-- Problem 5
------------------------------------------------------------------------------

factor :: Integer -> [Integer]
factor x =
    let
        helper n divisor =
            if n == divisor then
                [n]
            else if n `mod` divisor == 0 then
                let
                    y = n `div` divisor
                in
                    divisor:(helper y divisor)
            else
                helper n (divisor + 1)
    in
        if x <= 0 then
            []
        else if x <= 2 then
            [x]
        else
            1:(helper x 2)

lcf :: [Integer] -> [Integer] -> [Integer]
lcf curated uncurated =
    (uncurated \\ curated) ++ curated

solve5 :: Integer
solve5 =
    product (foldr lcf [] ([factor x | x <- [1..20]]))

------------------------------------------------------------------------------
-- Problem 6
------------------------------------------------------------------------------

solve6 :: Integer
solve6 =
    let
        numbers = [1..100]
        squares = [x * x | x <- numbers]
    in
        (sum numbers) * (sum numbers) - (sum squares)

------------------------------------------------------------------------------
-- Problem 7
------------------------------------------------------------------------------

isPrime :: Integer -> Bool
isPrime n =
    let
        checkLoop i n =
            if i * i <= n then
                if n `mod` i == 0 || n `mod` (i + 2) == 0 then
                    False
                else
                    checkLoop (i + 6) n
            else
                True
    in
        if n <= 3 then
            n > 1
        else if n `mod` 2 == 0 || n `mod` 3 == 0 then
            False
        else
            checkLoop 5 n

nthPrime :: Integer -> Integer
nthPrime n =
    let
        checkLoop rem x =
            if isPrime x then
                if rem <= 1 then
                    x
                else
                    checkLoop (rem - 1) (x + 1)
            else
                checkLoop rem (x + 1)
    in
        checkLoop n 2

solve7 :: Integer
solve7 =
    nthPrime 10001

------------------------------------------------------------------------------
-- Problem 8
------------------------------------------------------------------------------

streamSum :: Integer -> [Integer] -> [Integer] -> Integer
streamSum maxProduct held stream =
    let
        capacity = 13
    in
        case stream of
            [] ->
                maxProduct
            head:tail ->
                if (length held) == capacity then
                    let
                        newHeld = head:(take 12 held)
                        newProduct = product newHeld
                    in
                        if newProduct > maxProduct then
                            streamSum newProduct newHeld tail
                        else
                            streamSum maxProduct newHeld tail
                else
                    streamSum maxProduct (head:held) tail

solve8 :: Integer
solve8 =
    let
        seq =
            unlines [
                "73167176531330624919225119674426574742355349194934",
                "96983520312774506326239578318016984801869478851843",
                "85861560789112949495459501737958331952853208805511",
                "12540698747158523863050715693290963295227443043557",
                "66896648950445244523161731856403098711121722383113",
                "62229893423380308135336276614282806444486645238749",
                "30358907296290491560440772390713810515859307960866",
                "70172427121883998797908792274921901699720888093776",
                "65727333001053367881220235421809751254540594752243",
                "52584907711670556013604839586446706324415722155397",
                "53697817977846174064955149290862569321978468622482",
                "83972241375657056057490261407972968652414535100474",
                "82166370484403199890008895243450658541227588666881",
                "16427171479924442928230863465674813919123162824586",
                "17866458359124566529476545682848912883142607690042",
                "24219022671055626321111109370544217506941658960408",
                "07198403850962455444362981230987879927244284909188",
                "84580156166097919133875499200524063689912560717606",
                "05886116467109405077541002256983155200055935729725",
                "71636269561882670428252483600823257530420752963450"
            ]
        pureSeq = [x | x <- seq, not (x `elem` "\n")]
    in
        streamSum (-1) [] (map digitToInteger pureSeq)

------------------------------------------------------------------------------
-- Problem 9
------------------------------------------------------------------------------

checkB :: Integer -> Integer -> Maybe [Integer]
checkB a b =
    if a + b < 1000 then
        let
            c = 1000 - a - b
        in
            if a * a + b * b == c * c then
                Just [a, b, c]
            else
                checkB a (b + 1)
    else
        Nothing

checkA :: Integer -> [Integer]
checkA a =
    if a < 1000 then
        case checkB a 1 of
            Just answer -> answer
            Nothing -> checkA (a + 1)
    else
        [0, 0, 0]

solve9 :: Integer
solve9 =
    product (checkA 1)

------------------------------------------------------------------------------
-- Problem 17
------------------------------------------------------------------------------

solve17 :: Integer
solve17 =
    0
    -- get decimal place
    --  hundred     => 7
    --  thousand    => 8
    -- map the rest

------------------------------------------------------------------------------
-- Problem 20
------------------------------------------------------------------------------

solve20 :: Integer
solve20 =
    digitalSum (fac 100)

------------------------------------------------------------------------------
-- Problem 22
------------------------------------------------------------------------------

solve22 :: Integer
solve22 =
    let
        filepath = "../files/p022_names.txt"
    in
        0
