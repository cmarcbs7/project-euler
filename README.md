# Project Euler
My [Project Euler](https://projecteuler.net/archives) solutions in Haskell.

## License
This software and associated documentation files are licensed under the
[MIT License](https://opensource.org/licenses/MIT).

This file may not be copied, modified, or distributed except according to
those terms.
